﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Lidgren.Network;

namespace IslandSurvivalServer.Library
{
    public class NetworkedPlayer
    {
        private readonly object lockobject = new object();
        private float plyspeed = 2f;

        public NetworkedPlayer(string name, NetConnection netc)
        {
            Name = name;
            Connection = netc;
            Position = new Vector2(0f, 0f);
            Velocity = new Vector2(0f, 0f);
            State = new PlayerState(PlayerAction.Idle, Position.X, Position.Y);
        }

        public NetworkedPlayer(string name, NetConnection netc, Vector2 pos)
        {
            Name = name;
            Connection = netc;
            Position = pos;
            Velocity = new Vector2(0f, 0f);
            State = new PlayerState(PlayerAction.Idle, Position.X, Position.Y);
        }

        public void Update(int dt) //Called from Update thread
        {
            float delta = (float)dt / 1000f;
            Vector2 ddist = new Vector2(0f,0f);
            switch(State.PlayerAction)
            {
                case (PlayerAction.Idle):
                    break;
                case (PlayerAction.Walking):
                    Velocity = (State.Target - Position) / ((State.Target - Position).Length()) * plyspeed; //normalized vector
                    ddist = Velocity * delta;
                    break;
                case (PlayerAction.Running):
                    Velocity = (State.Target - Position) / ((State.Target - Position).Length()) * plyspeed;
                    ddist = Velocity * delta * 2f; //running is faster
                    break;
                case (PlayerAction.Using):
                    break;
            }

            if ((State.Target - Position).LengthSquared() > ddist.LengthSquared())
                Position += ddist;
            else
            {
                Position = State.Target;
                State = new PlayerState(PlayerAction.Idle);
            }
        }

        private void StopReachedDestination()
        {

        }

        public bool TryStartWalking(float x, float y)
        {
            if(true) //canmove
            {
                this.State = new PlayerState(PlayerAction.Walking, x, y);
                return true;
            }
            return false;
        }

        private PlayerState _State;
        public PlayerState State
        {
            get
            {
                lock (this.lockobject)
                {
                    return this._State;
                }
            }
            set
            {
                lock (this.lockobject)
                {
                    //TODO: check if can enter state
                    this._State = value;
                }
            }
        }

        private string _Name;
        public string Name
        {
            get
            {
                lock (this.lockobject)
                {
                    return this._Name;
                }
            }
            set
            {
                lock (this.lockobject)
                {
                    this._Name = value;
                }
            }
        }

        private NetConnection _Connection;
        public NetConnection Connection
        {
            get
            {
                lock (this.lockobject)
                {
                    return this._Connection;
                }
            }
            set
            {
                lock (this.lockobject)
                {
                    this._Connection = value;
                }
            }
        }

        private Vector2 _Position;
        public Vector2 Position
        {
            get
            {
                lock (this.lockobject)
                {
                    return this._Position;
                }
            }
            set
            {
                lock (this.lockobject)
                {
                    this._Position = value;
                }
            }
        }

        private Vector2 _Velocity;
        public Vector2 Velocity
        {
            get
            {
                lock (this.lockobject)
                {
                    return this._Velocity;
                }
            }
            set
            {
                lock (this.lockobject)
                {
                    this._Velocity = value;
                }
            }
        }
    }
}
