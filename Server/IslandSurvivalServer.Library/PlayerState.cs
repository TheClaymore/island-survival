﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace IslandSurvivalServer.Library
{
    public class PlayerState
    {

        private PlayerAction playeraction;
        private Vector2 target;
        private string targetKey;

        public PlayerState(PlayerAction pa, params object[] args)
        {
            playeraction = pa;
            target = new Vector2(0f, 0f);
            targetKey = "";
            switch (pa)
            {
                case (PlayerAction.Idle):
                    break;
                case (PlayerAction.Walking):
                    target = new Vector2((float)args[0], (float)args[1]);
                    break;
                case (PlayerAction.Running):
                    target = new Vector2((float)args[0], (float)args[1]);
                    break;
                case (PlayerAction.Using):
                    targetKey = (string)args[0];
                    break;
            }
        }

        public PlayerAction PlayerAction { get => playeraction; }
        public Vector2 Target { get => target; }
        public string TargetKey { get => targetKey; }
    }
}
