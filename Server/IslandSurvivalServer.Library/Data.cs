﻿namespace IslandSurvivalServer.Library
{
    public enum PacketType
    {
        Login,
        Logout,

        ClientSendMoveCommand,
        ServerUpdatePlayersMovement,

        ServerAnotherPlayerConnected,
        ServerAnotherPlayerDisconnected,
    }

    public enum PlayerAction
    { 
        Idle,
        Walking,
        Running,
        Using,
    }
}

