﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace IslandSurvivalServer.Server
{
    public class UpdateLoop
    {
        public bool DoLoop { get; set; }
        public int UpdateDt { get; set; }

        WorldData _data;
        Thread _loop;

        public UpdateLoop(WorldData data)
        {
            this._data = data;
            UpdateDt = 100; //milliseconds - 10 updates per second
        }

        public void Start()
        {
            DoLoop = true;
            _loop = new Thread(Loop);
            _loop.Start();
        }

        private void Loop()
        {
            Console.WriteLine("Update thread starting");
            DateTime last;
            double dt;
            int tosleep;
            while (DoLoop)
            {
                last = DateTime.Now;

                _data.Update(UpdateDt); //actual update

                dt = DateTime.Now.Subtract(last).TotalSeconds;
                tosleep = UpdateDt - (int)(dt * 1000);
                tosleep = tosleep > 0 ? tosleep : 0;
                //Console.WriteLine(tosleep.ToString());
                Thread.Sleep(tosleep);
            }
        }
    }
}
