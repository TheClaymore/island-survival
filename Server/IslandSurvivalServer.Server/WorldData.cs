﻿using IslandSurvivalServer.Library;
using Lidgren.Network;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;


namespace IslandSurvivalServer.Server
{
    public class WorldData
    {

        private ConcurrentDictionary<string, NetworkedPlayer> players;
        private Random rnd;

        public bool NewInfoToUpdate {get; set;}

        public WorldData()
        {
            players = new ConcurrentDictionary<string, NetworkedPlayer>();
            rnd = new Random();
            NewInfoToUpdate = false;
        }

        public void Update(int dt) //Called from update thread, dt in milliseconds
        {
            //UpdateShips
            foreach (var ply in players)
            {
                ply.Value.Update(dt);
            }
            //UpdateEntities
            //UpdateIslands
            //UpdateEvents,NPCS,etc.
            this.NewInfoToUpdate = true;
        }

        public NetworkedPlayer RemovePlayer(string name)
        {
            if (!PlayerExists(name))
                return null;
            NetworkedPlayer ply;
            bool success = players.TryRemove(name, out ply);
            if (success)
                return ply;
            else
                return null;

        }

        /*public void UpdatePlayer(string name, NetworkedPlayer ply)
        {

        }*/

        public NetworkedPlayer AddPlayer(string name, NetConnection netc)
        {
            if (PlayerExists(name))
                return null;
            var ply = new NetworkedPlayer(name, netc);
            bool success = players.TryAdd(name, ply);
            if (success)
                return ply;
            else
                return null;
        }

        public int GetPlayerCount()
        {
            return players.Count;
        }

        public bool PlayerExists(string name)
        {
            return players.ContainsKey(name);
        }

        public NetworkedPlayer GetPlayer(string name)
        {
            if (!PlayerExists(name))
                return null;
            NetworkedPlayer ply;
            bool gotit = players.TryGetValue(name, out ply);
            if (gotit)
                return ply;
            else
                return null;
        }
    }
}
