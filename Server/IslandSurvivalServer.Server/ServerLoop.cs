﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using IslandSurvivalServer.Library;
using Lidgren.Network;

namespace IslandSurvivalServer.Server
{
    public class ServerLoop
    {

        private WorldData worldData;
        private Dictionary<NetConnection, string> plyNames;

        public int Port { get; set; }

        public ServerLoop(WorldData data)
        {
            this.worldData = data;
            this.Port = 25565;
            this.plyNames = new Dictionary<NetConnection, string>();
        }

        public ServerLoop(WorldData data, int newport)
        {
            this.worldData = data;
            this.Port = newport;
            this.plyNames = new Dictionary<NetConnection, string>();
        }

        public void Start(bool debugConsole)
        {
            var random = new Random();
            Console.WriteLine("Server starting at " + Port.ToString());
            var config = new NetPeerConfiguration("networkGame") { Port = Port };
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            var server = new NetServer(config);
            server.Start();
            Console.WriteLine("Server started");

            while (true)
            {
                byte data;
                NetIncomingMessage inc;
                while((inc = server.ReadMessage()) != null)
                {
                    switch (inc.MessageType)
                    {
                        case NetIncomingMessageType.Error:
                            break;
                        case NetIncomingMessageType.WarningMessage:
                            break;
                        case NetIncomingMessageType.ConnectionApproval:
                            Console.Write("New connection attempt...");
                            data = inc.ReadByte();
                            if (data == (byte)PacketType.Login)
                            {
                                inc.SenderConnection.Approve();
                                CreatePlayer(inc);
                                var outmsg = server.CreateMessage();
                                outmsg.Write((byte)PacketType.Login);
                                outmsg.Write(true);
                                var result = server.SendMessage(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                                Console.WriteLine("accepted from " + plyNames[inc.SenderConnection] + " [Total:" + plyNames.Count.ToString() + "/" + this.worldData.GetPlayerCount().ToString() + "]");
                            }
                            else
                            {
                                Console.WriteLine("denied");
                                inc.SenderConnection.Deny("Didn't send correct connect information");
                            }
                            break;

                            /*
                             * 
                             * The DATA below
                             * 
                             * 
                             */

                            //TODO:Add all messages
                        case NetIncomingMessageType.Data:
                            data = inc.ReadByte();
                            //Send to some class, working with commands
                            if (data == (byte)PacketType.Logout)
                            {
                                if (PlayerKeyExists(inc.SenderConnection))
                                {
                                    Console.Write("User logged out: " + plyNames[inc.SenderConnection]);
                                    RemovePlayer(inc);
                                    Console.WriteLine(" [Total:" + plyNames.Count.ToString() + "/" + this.worldData.GetPlayerCount().ToString() + "]");
                                    inc.SenderConnection.Disconnect("Logout by user");
                                }
                            }
                            if (data == (byte)PacketType.ClientSendMoveCommand)
                            {
                                var plytowalk = worldData.GetPlayer(plyNames[inc.SenderConnection]); 
                                plytowalk.TryStartWalking(inc.ReadFloat(), inc.ReadFloat()); //x,y
                            }
                            break;

                            /*
                             * The DATA has ended
                             */

                        case NetIncomingMessageType.StatusChanged:
                            //Console.WriteLine(inc.SenderConnection.Status);
                            if (inc.SenderConnection.Status == NetConnectionStatus.Connected)
                            {

                            }
                            if (inc.SenderConnection.Status == NetConnectionStatus.Disconnected)
                            {
                                if (PlayerKeyExists(inc.SenderConnection))
                                {
                                    Console.Write("Disconnected :[" + plyNames[inc.SenderConnection] + "]");
                                    RemovePlayer(inc);
                                    Console.WriteLine(" [Total:" + plyNames.Count.ToString() + "/" + this.worldData.GetPlayerCount().ToString() + "]");
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    server.Recycle(inc);
                }

                if (!worldData.NewInfoToUpdate) continue; //if no new data to update
                worldData.NewInfoToUpdate = false;

                //Update players about their surroundings
                //About ships
                //About players

                foreach(var ply in plyNames)
                {
                    var posmsg = server.CreateMessage();
                    posmsg.Write((byte)PacketType.ServerUpdatePlayersMovement); 
                    //number of players to receive
                    //list of other players
                    //for (int i = 0; i < plyNames.Count; i++)
                    //{
                    var otherPlyNames = plyNames.Values; //get based on distance etc.
                    posmsg.WriteVariableInt32(otherPlyNames.Count);
                    foreach(string othername in otherPlyNames)
                    {
                        var targetply = worldData.GetPlayer(othername);
                        posmsg.Write((string)othername);

                        posmsg.Write((byte)targetply.State.PlayerAction);

                        posmsg.Write((float)targetply.Position.X);
                        posmsg.Write((float)targetply.Position.Y);

                        posmsg.Write((float)targetply.Velocity.X);
                        posmsg.Write((float)targetply.Velocity.Y);
                    }
                        
                    //}
                    server.SendMessage(posmsg, ply.Key, NetDeliveryMethod.ReliableSequenced, 0);
                }

                //Objects
                //Islands

            }
        }

        private bool PlayerKeyExists(NetConnection con)
        {
            return plyNames.ContainsKey(con);
        }

        private void CreatePlayer(NetIncomingMessage inc)
        {
            var Connection = inc.SenderConnection;
            var Name = inc.ReadString();
            worldData.AddPlayer(Name, Connection);
            if (!PlayerKeyExists(Connection))
                plyNames.Add(Connection, Name);
        }

        private void RemovePlayer(NetIncomingMessage inc)
        {
            var Connection = inc.SenderConnection;
            var Name = plyNames[Connection];
            worldData.RemovePlayer(Name);
            if(PlayerKeyExists(Connection))
                plyNames.Remove(Connection);
        }
    }
}
