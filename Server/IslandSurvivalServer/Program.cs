﻿using System;
using IslandSurvivalServer.Server;
using IslandSurvivalServer.Library;

namespace IslandSurvivalServer
{
    class Program
    {
        static void Main()
        {
            var data = new WorldData();
            var server = new ServerLoop(data);
            var update = new UpdateLoop(data) { UpdateDt = 100 };

            update.Start(); //Update thread
            server.Start(true); //Main thread
        }
    }
}
