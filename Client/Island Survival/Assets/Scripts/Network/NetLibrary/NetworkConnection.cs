﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using Lidgren.Network;

class NetworkConnection : MonoBehaviour
{
    private NetClient _client;
    private int _port;
    private NetConnection _server;

    public bool isConnected;
    public bool connectionFailed;

    public DateTime connectStart;

    public readonly object networkLock = new object();

    public void Start()
    {
        Connect();
    }

    void Connect()
    {
        var manager = GetComponent<NetworkManager>();
        _port = manager.port;
        connectionFailed = false;
        isConnected = false;
        var loginInforation = new NetworkLoginInformation() { Name = manager.nickname };

        var config = new NetPeerConfiguration(manager.netPeerName);
        //config.AutoFlushSendQueue = true;
        _client = new NetClient(config);
        _client.Start();

        var outmsg = _client.CreateMessage();
        outmsg.Write((byte)PacketType.Login);
        outmsg.WriteAllProperties(loginInforation);
        _client.Connect(manager.ip, _port, outmsg);

        connectStart = DateTime.Now;
    }

    public void Update()
    {
        if (disconnect) return;

        if(_client.ConnectionStatus != NetConnectionStatus.Connected)
            TryConnect();
        else
            ReadMessages();

        if(Input.GetKeyDown(KeyCode.X))
        {
            Disconnect();
        }
    }

    public bool CheckConnection()
    {
        return _client.ConnectionStatus == NetConnectionStatus.Connected;
    }

    void ReadMessages()
    {
        byte data;
        if (_client.ConnectionStatus != NetConnectionStatus.Connected) return;
        NetIncomingMessage inc;
        if ((inc = _client.ReadMessage()) == null) return;
        switch (inc.MessageType)
        {
            case NetIncomingMessageType.Data:
                data = inc.ReadByte();
                if(data == (byte)PacketType.ServerUpdatePlayersMovement)
                {
                    List<PlayerRawData> allPlyRaw = new List<PlayerRawData>();
                    int plycount = inc.ReadVariableInt32();
                    for(int i = 0; i<plycount; i++)
                    {
                        allPlyRaw.Add(new PlayerRawData(
                            inc.ReadString(), //Name
                            (PlayerAction)inc.ReadByte(),  //State
                            inc.ReadFloat(), //x
                            inc.ReadFloat(), //y
                            inc.ReadFloat(), //vx
                            inc.ReadFloat() //vy
                        ));
                    }
                    GameObject.FindGameObjectWithTag("GameController").GetComponent<NetworkedPlayersController>().ReceiveRawPlayerData(allPlyRaw);
                }
                break;
        }
    }

    void TryConnect()
    {
        if (connectionFailed || isConnected || _client.ConnectionStatus == NetConnectionStatus.Connected) return;

        NetIncomingMessage inc;
        if ((inc = _client.ReadMessage()) == null) return;
        switch (inc.MessageType)
        {
            case NetIncomingMessageType.Data:

                var data = inc.ReadByte();
                if (data == (byte)PacketType.Login)
                {
                    var accepted = inc.ReadBoolean();
                    if (accepted)
                    {
                        isConnected = true;
                        _server = inc.SenderConnection;
                        return;
                    }
                    else
                    {
                        connectionFailed = true;
                        return;
                    }
                }
                else
                {
                    connectionFailed = true;
                    return;
                }
        }

    }

    private bool disconnect = false;

    public void SendMovementCommand(Vector3 towards)
    {
        var movcmdmsg = _client.CreateMessage();
        movcmdmsg.Write((byte)PacketType.ClientSendMoveCommand);
        movcmdmsg.Write(towards.x);
        movcmdmsg.Write(towards.z);
        _client.SendMessage(movcmdmsg, NetDeliveryMethod.ReliableSequenced, 0);
        _client.FlushSendQueue();
    }

    private void Disconnect()
    {
        disconnect = true;
        if (_client.ConnectionStatus == NetConnectionStatus.Connected)
        {
            var logoutmsg = _client.CreateMessage();
            logoutmsg.Write((byte)PacketType.Logout);
            _client.SendMessage(logoutmsg, NetDeliveryMethod.ReliableOrdered);
            _client.FlushSendQueue();

            _client.Disconnect("appexit");
            _client.FlushSendQueue();
        }
    }

    void OnApplicationQuit()
    {
        Disconnect();
    }
}
