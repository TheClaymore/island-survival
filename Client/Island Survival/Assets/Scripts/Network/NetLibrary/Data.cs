﻿public enum PacketType
{
    Login,
    Logout,

    ClientSendMoveCommand,
    ServerUpdatePlayersMovement,

    ServerAnotherPlayerConnected,
    ServerAnotherPlayerDisconnected,
}

public enum PlayerAction
{
    Idle,
    Walking,
    Running,
    Using,
}

public struct PlayerRawData{
    
    public PlayerRawData(string _name, PlayerAction _action, float _posx, float _posy, float _velx, float _vely)
    {
        nickname = _name;
        action = _action;
        x = _posx;
        y = _posy;
        vx = _velx;
        vy = _vely;
    }
    public string nickname { get; }
    public PlayerAction action { get; }
    public float x { get; }
    public float y { get; }
    public float vx { get; }
    public float vy { get; }
}