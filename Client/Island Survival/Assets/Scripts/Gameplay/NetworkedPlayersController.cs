﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkedPlayersController : MonoBehaviour
{
    public GameObject PlayerPrefab;

    private GameObject PlayerlistParent;
    private Dictionary<string, GameObject> NetworkedPlayerDict;

    // Start is called before the first frame update
    void Start()
    {
        PlayerlistParent = new GameObject();
        PlayerlistParent.name = "PlayerlistParent";
        NetworkedPlayerDict = new Dictionary<string, GameObject>();
    }

    private bool RawDataContainsName(List<PlayerRawData> allPlyRaw, string name)
    {
        foreach(var plyRaw in allPlyRaw)
        {
            if (plyRaw.nickname == name)
                return true;
        }
        return false;
    }

    public void ReceiveRawPlayerData(List<PlayerRawData> allPlyRaw)
    {
        //Remove deprecated players
        List<string> todelete = new List<string>();

        foreach (var entry in NetworkedPlayerDict)
        {
            if (!RawDataContainsName(allPlyRaw, entry.Key))
            {
                todelete.Add(entry.Key);
            }

        }

        foreach (var key in todelete)
        {
            GameObject.Destroy(NetworkedPlayerDict[key]); //Delete gobj
            NetworkedPlayerDict.Remove(key); //Remove from list
        }

        //Add new players
        foreach (var plyRaw in allPlyRaw)
        {
            if (NetworkedPlayerDict.ContainsKey(plyRaw.nickname)) continue;
            GameObject gobj = Instantiate(PlayerPrefab, new Vector3(plyRaw.x, 0f, plyRaw.y), transform.rotation);
            gobj.name = plyRaw.nickname;
            NetworkedPlayerDict.Add(plyRaw.nickname, gobj);
        }

        //Update existing players
        foreach (var plyRaw in allPlyRaw)
        {
            NetworkedPlayerDict[plyRaw.nickname].GetComponent<PlayerNetworkedMove>().ReceiveRawData(plyRaw);
            if(plyRaw.nickname == GameObject.FindGameObjectWithTag("NetworkController").GetComponent<NetworkManager>().nickname)
            {
                Camera.main.transform.gameObject.GetComponent<CameraFollow>().lookAt = NetworkedPlayerDict[plyRaw.nickname].transform;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
