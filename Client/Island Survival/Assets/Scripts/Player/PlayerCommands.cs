﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCommands : MonoBehaviour
{

    NetworkConnection netcon;

    private void Start()
    {
        netcon = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<NetworkConnection>();
    }

    // Update is called once per frame
    void Update()
    {
        if(netcon == null)
        {
            netcon = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<NetworkConnection>();
            if (netcon == null) return;
        }
        CheckMovementCommand();
    }

    private void CheckMovementCommand()
    {
        if(netcon.CheckConnection())
        {
            if (Input.GetMouseButtonDown(1))
            {
                //create a ray cast and set it to the mouses cursor position in game
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    //Debug.Log(hit.point);
                    netcon.SendMovementCommand(hit.point);
                }
            }
        }
    }
}
