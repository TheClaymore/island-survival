﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public Transform lookAt;
    public Vector3 lookOffset;
    public bool shouldFollow = true;
    public float cameraSens = 10f;
    public bool mouseShouldMove = true;

    private Vector3 worldUp = new Vector3(0, 1, 0);
    
    private Vector3 posOffset;

    private float cameraAngle = 0f;
    private float cameraRadius = 4f;
    private float cameraHeight = 5f;


    void LateUpdate()
    {
        if (!shouldFollow)
            return;

        GetPlayerInput();
        posOffset = GetCameraOffset();
        this.transform.position = Vector3.Lerp(this.transform.position, lookAt.position + posOffset, 1.5f);
        this.transform.LookAt(lookAt.position + lookOffset, worldUp);
    }

    private Vector3 prevMouse;

    void GetPlayerInput()
    {
        if (!mouseShouldMove)
            return; //No input if mouse isnt under our control

        if (Input.GetMouseButtonDown(0))
            prevMouse = Input.mousePosition;
        if(Input.GetMouseButton(0))
        {
            cameraAngle -= (Input.mousePosition.x - prevMouse.x) * cameraSens;
            prevMouse = Input.mousePosition;
        }

        cameraRadius -= Input.mouseScrollDelta.y * cameraSens * 100f;
        cameraRadius = Mathf.Clamp(cameraRadius, 3f, 6f);

        cameraHeight = 1.5f*cameraRadius;


        if (cameraAngle > Mathf.PI)
        {
            cameraAngle -= 2 * Mathf.PI;
        }
        if(cameraAngle < -Mathf.PI)
        {
            cameraAngle += 2 * Mathf.PI;
        }
    }

    Vector3 GetCameraOffset()
    {
        Vector3 result = new Vector3(cameraRadius * Mathf.Cos(cameraAngle), cameraHeight, cameraRadius * Mathf.Sin(cameraAngle));
        return result;
    }
}
