﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworkedMove : MonoBehaviour
{
    CharacterController playerController;

    private Vector3 lerpstartpos;
    private Vector3 lerpendpos;
    private float lerpdur;
    private bool shouldmove;
    private Vector3 worldUp;

    void Start()
    {
        worldUp = new Vector3(0f, 1f, 0f);
        playerController = GetComponent<CharacterController>();
        shouldmove = false;
    }

    private float lerpcurr;
    private void Update()
    {
        if(shouldmove)
        {
            lerpcurr += Time.deltaTime / lerpdur;
            transform.position = Vector3.Lerp(lerpstartpos, lerpendpos, lerpcurr);
            transform.LookAt(lerpendpos, worldUp);
        }
    }

    public void ReceiveRawData(PlayerRawData plydat)
    {
        if(plydat.action == PlayerAction.Walking)
        {
            
            lerpstartpos = transform.position;
            lerpendpos = new Vector3(plydat.x, 0, plydat.y);
            lerpdur = (lerpendpos - lerpstartpos).magnitude / new Vector3(plydat.vx, 0, plydat.vy).magnitude;
            shouldmove = true;
            lerpcurr = 0;
        }
        if(plydat.action == PlayerAction.Idle)
        {
            lerpstartpos = new Vector3(plydat.x, 0, plydat.y);
            lerpendpos = lerpstartpos;
            lerpdur = (lerpendpos - lerpstartpos).magnitude / new Vector3(plydat.vx, 0, plydat.vy).magnitude;
            shouldmove = false;
        }
        //var movespeed = new Vector3(plydat.vx, 0, plydat.vy);
        //playerController.Move(new Vector3(plydat.x - transform.position.x, 0f, plydat.y - transform.position.z));
    }
}
