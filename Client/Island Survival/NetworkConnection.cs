﻿using Lidgren.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class NetworkConnection
{
    private NetClient _client;

    public bool Start()
    {
        var loginInforation = new NetworkLoginInformation() { Name = "RandomName" };
        _client = new NetClient(new NetPeerConfiguration("networkGame") { Port = 9982 });
        _client.Start();
        var outmsg = _client.CreateMessage();
        outmsg.Write((byte)PacketType.Login);
        outmsg.WriteAllProperties(loginInforation);
        _client.ConXnect("localhost", 9982, outmsg);
        return EstablishInfo();
    }

    private bool EstablishInfo()
    {
        var time = DateTime.Now;
        NetIncomingMessage inc;
        while (true)
        {
            if (DateTime.Now.Subtract(time).Seconds > 5)
            {
                return false;
            }
            if ((inc = _client.ReadMessage()) == null) continue;

            switch (inc.MessageType)
            {
                case NetIncomingMessageType.Data:
                    var data = inc.ReadByte();
                    if (data == (byte)PacketType.Login)
                    {
                        var accepted = inc.ReadBoolean();
                        if (accepted)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }
        }

    }

}
